# pylib
import unittest
from unittest import mock

# local
import minidi

class BaseInjectable(minidi.Injectable):
	def executeBaseFunction(self) -> str:
		return 'Hello, World!'
# class BaseInjectable(minidi.Injectable)

class SuperInjectable(minidi.Injectable):
	pBaseInjectable: BaseInjectable

	def afterInit(self):
		self.pBaseInjectable.executeBaseFunction()
# class SuperInjectable(minidi.Injectable)

class TestAfterInit(unittest.TestCase):
	def test(self):
		pBaseInjectable: BaseInjectable = minidi.get(BaseInjectable)
		pBaseInjectable.executeBaseFunction = mock.Mock()

		pSuperInjectable: SuperInjectable = minidi.get(SuperInjectable)
		self.assertIs(pSuperInjectable.pBaseInjectable, pBaseInjectable)

		pBaseInjectable.executeBaseFunction.assert_called_once()
	# def test(self)
# class TestAfterInit(unittest.TestCase)