# pylib
import unittest

# local
import minidi

class BaseInjectable(minidi.Injectable):
	def executeBaseFunction(self) -> str:
		return 'Hello, World!'
# class BaseInjectable(minidi.Injectable)

class NonDefaultInjectable(minidi.Injectable):
	pBaseInjectable: BaseInjectable

	def __init__(self, pBaseInjectable: BaseInjectable):
		self.pBaseInjectable = pBaseInjectable

	def executeNonDefaultFunction(self) -> str:
		return self.pBaseInjectable.executeBaseFunction()
# class NonDefaultInjectable(minidi.Injectable)

class TestNonDefaultInjectable(unittest.TestCase):
	def test(self):
		pBaseInjectable = BaseInjectable()
		pNonDefaultInjectable = NonDefaultInjectable(pBaseInjectable)
		self.assertEqual(pNonDefaultInjectable.executeNonDefaultFunction(), 'Hello, World!')

		self.assertRaises(ValueError, minidi.get, NonDefaultInjectable)
	# def test(self)
# class TestNonDefaultInjectable(unittest.TestCase)