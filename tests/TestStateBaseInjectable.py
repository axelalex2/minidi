# pylib
# pylib
import unittest

# local
import minidi

class StateBaseInjectable(minidi.Injectable):
	_msg: str = 'Hello, World!'
	
	def executeBaseFunction(self) -> str:
		return self._msg
# class StateBaseInjectable(minidi.Injectable)

class AnotherStateBaseInjectable(minidi.Injectable):
	_msg: str = 'How are you doing?'

	def executeAnotherBaseFunction(self) -> str:
		return self._msg
# class AnotherStateBaseInjectable(minidi.Injectable)

class SuperInjectable(minidi.Injectable):
	pStateBaseInjectable: StateBaseInjectable
	pAnotherStateBaseInjectable: AnotherStateBaseInjectable

	def executeSuperFunction(self) -> list:
		return [
			self.pStateBaseInjectable.executeBaseFunction(),
			self.pAnotherStateBaseInjectable.executeAnotherBaseFunction()
		]
	# def executeSuperFunction(self) -> list
# class BaseInjectable(minidi.Injectable)

class TestStateBaseInjectable(unittest.TestCase):
	def test(self):
		pSuperInjectable = SuperInjectable()
		pSuperInjectable.pStateBaseInjectable = StateBaseInjectable()
		pSuperInjectable.pAnotherStateBaseInjectable = AnotherStateBaseInjectable()
		self.assertIsInstance(pSuperInjectable, SuperInjectable)
		expectedList = ['Hello, World!', 'How are you doing?']
		self.assertListEqual(pSuperInjectable.executeSuperFunction(), expectedList)

		self.assertRaises(ValueError, minidi.get, SuperInjectable)
		self.assertRaises(ValueError, minidi.get, StateBaseInjectable)
		self.assertRaises(ValueError, minidi.get, AnotherStateBaseInjectable)
	# def test(self)
# class TestStateBaseInjectable(unittest.TestCase)