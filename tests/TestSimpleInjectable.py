# pylib
import unittest

# local
import minidi

class SimpleInjectable(minidi.Injectable):
	def executeFunction(self) -> str:
		return 'Hello, World!'
# class SimpleInjectable(minidi.Injectable)

class TestSimpleInjectable(unittest.TestCase):
	def test(self):
		pSimpleInjectable = minidi.get(SimpleInjectable)
		self.assertIsInstance(pSimpleInjectable, SimpleInjectable)
		self.assertEqual(pSimpleInjectable.executeFunction(), 'Hello, World!')

		pSameInstance = minidi.get(SimpleInjectable)
		self.assertEqual(pSameInstance, pSimpleInjectable)
	# def test(self)
# class TestSimpleInjectable(unittest.TestCase)