# pylib
import unittest

# local
import minidi

class BaseInjectable(minidi.Injectable):
	def executeBaseFunction(self) -> str:
		return 'Hello, World!'
# class BaseInjectable(minidi.Injectable)

class AnotherBaseInjectable(minidi.Injectable):
	def executeAnotherBaseFunction(self) -> str:
		return 'How are you doing?'
# class AnotherBaseInjectable(minidi.Injectable)

class SuperInjectable(minidi.Injectable):
	pBaseInjectable: BaseInjectable
	pAnotherBaseInjectable: AnotherBaseInjectable

	def executeSuperFunction(self) -> list:
		return [
			self.pBaseInjectable.executeBaseFunction(),
			self.pAnotherBaseInjectable.executeAnotherBaseFunction()
		]
	# def executeSuperFunction(self) -> list
# class SuperInjectable(minidi.Injectable)

class TestComplexInjectable(unittest.TestCase):
	def test(self):
		pSuperInjectable = minidi.get(SuperInjectable)
		self.assertIsInstance(pSuperInjectable, SuperInjectable)
		expectedList = ['Hello, World!', 'How are you doing?']
		self.assertListEqual(pSuperInjectable.executeSuperFunction(), expectedList)

		pBaseInjectable = minidi.get(BaseInjectable)
		self.assertIsInstance(pBaseInjectable, BaseInjectable)
		self.assertEqual(pBaseInjectable, pSuperInjectable.pBaseInjectable)

		pAnotherBaseInjectable = minidi.get(AnotherBaseInjectable)
		self.assertIsInstance(pAnotherBaseInjectable, AnotherBaseInjectable)
		self.assertEqual(pAnotherBaseInjectable, pSuperInjectable.pAnotherBaseInjectable)
	# def test(self)
# class TestComplexInjectable(unittest.TestCase)