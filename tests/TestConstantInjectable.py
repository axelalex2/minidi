# pylib
import unittest

# local
import minidi

class TypelessConstantInjectable(minidi.Injectable):
	SOME_CONSTANT = 'some constant'

	def getSomeConstant(self) -> str:
		return self.SOME_CONSTANT
# class TypelessConstantInjectable(minidi.Injectable)

class TypedConstantInjectable(minidi.Injectable):
	SOME_CONSTANT: str = 'some constant'

	def getSomeConstant(self) -> str:
		return self.SOME_CONSTANT
# class TypedConstantInjectable(minidi.Injectable)

class TestConstantInjectable(unittest.TestCase):
	def test(self):
		pTypelessConstantInjectable = TypelessConstantInjectable()
		self.assertEqual(pTypelessConstantInjectable.getSomeConstant(), 'some constant')

		pTypelessConstantInjectable = minidi.get(TypelessConstantInjectable)
		self.assertIsInstance(pTypelessConstantInjectable, TypelessConstantInjectable)
		self.assertEqual(pTypelessConstantInjectable.getSomeConstant(), 'some constant')

		def reassignConstant():
			pTypelessConstantInjectable.SOME_CONSTANT = 'some new constant'
		self.assertRaises(TypeError, reassignConstant)

		pTypedConstantInjectable = TypedConstantInjectable()
		self.assertEqual(pTypedConstantInjectable.getSomeConstant(), 'some constant')

		self.assertRaises(ValueError, minidi.get, TypedConstantInjectable)
	# def test(self)
# class TestConstantInjectable(unittest.TestCase)