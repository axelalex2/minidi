# pylib
from abc import abstractmethod
import unittest

# local
import minidi

class InjectableInterface(minidi.Injectable):
	@abstractmethod
	def execute(self) -> str:
		pass
# class InjectableInterface(minidi.Injectable)

class InjectableInvalid(minidi.Injectable):
	def execute(self) -> str:
		return 'invalid'
# class InjectableInvalid(minidi.Injectable)

class InjectableLive(InjectableInterface):
	def execute(self) -> str:
		return 'live'
# class InjectableLive(InjectableInterface)

class InjectableTest(InjectableInterface):
	def execute(self) -> str:
		return 'test'
# class InjectableTest(InjectableInterface)

class TestSet(unittest.TestCase):
	def test_interface(self):
		self.assertRaises(ValueError, minidi.get, InjectableInterface)
	# def test_interface(self)

	def test_invalid(self):
		pInjectableInvalid = minidi.get(InjectableInvalid)
		self.assertRaises(ValueError, minidi.set, InjectableInterface, pInjectableInvalid)
	# def test_invalid(self)

	def test_live(self):
		pInjectableLive = minidi.get(InjectableLive)
		minidi.set(InjectableInterface, pInjectableLive)

		pInjectableInterface: InjectableInterface = minidi.get(InjectableInterface)
		self.assertEqual(pInjectableInterface.execute(), 'live')
		self.assertIs(pInjectableInterface, pInjectableLive)
	# def test_live(self)

	def test_test(self):
		pInjectableTest = minidi.get(InjectableTest)
		minidi.set(InjectableInterface, pInjectableTest)

		pInjectableInterface: InjectableInterface = minidi.get(InjectableInterface)
		self.assertEqual(pInjectableInterface.execute(), 'test')
		self.assertIs(pInjectableInterface, pInjectableTest)
	# def test_test(self)
# class TestSet(unittest.TestCase)