# pylib
import unittest

# local
import minidi

class NonInjectable:
	def executeFunction(self) -> str:
		return 'Hello, World!'
# class NonInjectable

class TestNonInjectable(unittest.TestCase):
	def test(self):
		pNonInjectable = NonInjectable()
		self.assertEqual(pNonInjectable.executeFunction(), 'Hello, World!')

		self.assertRaises(ValueError, minidi.get, NonInjectable)
	# def test(self)
# class TestNonInjectable(unittest.TestCase)