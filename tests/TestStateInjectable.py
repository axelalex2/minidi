# pylib
import unittest

# local
import minidi

class StateInjectable(minidi.Injectable):
	_id: int = 42
	_state: str = 'Hello, World!'

	def getId(self) -> int:
		return self._id
	
	def getState(self) -> str:
		return self._state
# class StateInjectable(minidi.Injectable)

class TestStateInjectable(unittest.TestCase):
	def test(self):
		pStateInjectable = StateInjectable()
		self.assertEqual(pStateInjectable.getId(), 42)
		self.assertEqual(pStateInjectable.getState(), 'Hello, World!')

		self.assertRaises(ValueError, minidi.get, StateInjectable)
	# def test(self)
# class TestStateInjectable(unittest.TestCase)