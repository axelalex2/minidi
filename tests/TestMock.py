# pylib
import unittest
from unittest import mock

# local
import minidi

class BaseInjectable(minidi.Injectable):
	def executeBaseFunction(self) -> str:
		return 'Complicated stuff ...'
# class BaseInjectable(minidi.Injectable)

class AnotherBaseInjectable(minidi.Injectable):
	def executeAnotherBaseFunction(self) -> str:
		return 'More complicated stuff ...'
# class AnotherBaseInjectable(minidi.Injectable)

class SuperInjectable(minidi.Injectable):
	pBaseInjectable: BaseInjectable
	pAnotherBaseInjectable: AnotherBaseInjectable

	def executeSuperFunction(self) -> list:
		return [
			self.pBaseInjectable.executeBaseFunction(),
			self.pAnotherBaseInjectable.executeAnotherBaseFunction()
		]
	# def executeSuperFunction(self) -> list
# class BaseInjectable(minidi.Injectable)

class TestComplexInjectable(unittest.TestCase):
	def test(self):
		pBaseInjectable = BaseInjectable()
		pBaseInjectable.executeBaseFunction = mock.Mock(return_value='Hello, World!')
		pAnotherBaseInjectable = AnotherBaseInjectable()
		pAnotherBaseInjectable.executeAnotherBaseFunction = mock.Mock(return_value='How are you doing?')
		pSuperInjectable = SuperInjectable()
		pSuperInjectable.pBaseInjectable = pBaseInjectable
		pSuperInjectable.pAnotherBaseInjectable = pAnotherBaseInjectable
		
		expectedList = ['Hello, World!', 'How are you doing?']
		self.assertListEqual(pSuperInjectable.executeSuperFunction(), expectedList)
	# def test(self)
# class TestComplexInjectable(unittest.TestCase)